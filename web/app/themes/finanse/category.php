<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

        <section id="mainContent">
            <div class="container">
            
                    <div class="page-header">
                    <h1 class="text-uppercase"><?=single_cat_title();?></h1>
                    </div>

                    <div class="row">
                    <?php
            		while ( have_posts() ) : the_post();

            			get_template_part( 'template-parts/box_article_category' );

            		endwhile; // End of the loop.
            		?>
                    </div>

                    <?=pagination_bar();?>
            </div>
        </section>
<?php

get_footer();
