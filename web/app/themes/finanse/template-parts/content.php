<?php
/**
 * Template part for displaying posts.
 */

?>

<h1><?=get_the_title();?></h1>

<div class="content_meta"><?=the_date('j F Y');?> | Kategoria: <?=get_the_category_list(', ');?> </div>

<?php
$content_photo = get_field('post_content_photo');

if($content_photo)
{
    echo '<img src="'.$content_photo.'" class="img-responsive article_main_img">';
}
else
{
    the_post_thumbnail('800x250',array('class'=>'img-responsive article_main_img'));     
}

the_content();
?>

<div class="socials">
    <div class="social_header">Udostępnij:</div>
    <a href="#" class="share-facebook"><i class="icon-facebook"></i></a>
    <a href="#" class="share-twitter"><i class="icon-twitter"></i></a>
    <a href="#" class="share-google-plus"><i class="icon-google-plus"></i></a>
</div>