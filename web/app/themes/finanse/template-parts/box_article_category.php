<?php
/**
 * Template part for displaying posts.
 */



?>
<div class="col-sm-6 col-md-4 boxArticle">
    <div class="thumbnail">
      <a href="<?=get_permalink();?>">

        <?php
        the_post_thumbnail('360x200',array('class'=>'img-responsive'));
        ?>

      </a>
      <div class="caption">
        <h3><a href="<?=get_permalink();?>"><?=get_the_title();?></a></h3>
        <div class="boxArticleMeta"><?=get_the_time('j F Y');?> | <?=get_the_category_list(', ');?> </div>
        <p><?=get_the_excerpt();?></p>
        <a href="<?=get_permalink();?>" class="btn btn-success pull-right" role="button">Zobacz więcej »</a>
        <div class="clearfix"></div>
      </div>
    </div>
</div>
