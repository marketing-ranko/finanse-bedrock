<?php
/*
Template Name: Oferty
*/

$content = '';
while ( have_posts() ) : the_post();
$content .= get_the_content();
endwhile;

//[sowa_lista_ofert typ_oferty="b" limit="20"]

if(strpos($content,'sowa_lista_ofert')!==false) {        
   
   preg_match('/typ_oferty="(.)"/',$content,$matches);   
   $offer_type = $matches[1];
   $offers_count = get_offers_count($offer_type);   
}

get_header(); ?>
<section id="mainContent">
    <div class="container">                   
        <h1><?=str_replace("{{count}}",$offers_count,get_the_title()); ?></h1>
                
        <div class="page_content">
            <?php
            echo do_shortcode($content);
            ?>
        </div>    
        
    </div>
</section>
<?php
get_footer();