<?php
/*
Template Name: Strona główna
*/

get_header();
$count_arr = count_offers();
$oferty_suma = array_sum($count_arr);
?>
        <section id="heroZone">
            <div class="container">
                <div class="heroTitles">
                    <h1><?=get_the_title();?></h1>
                    <?=str_replace('{{oferty_suma}}',$oferty_suma,get_field('glowna_podtytul')); ?>
                </div>
                <div class="heroBoxes">
                    <div class="row">
                        <?php
                        $boxy_html = get_field('glowna_boxy');

                        $boxy_html = str_replace(array("{{oferty_b}}", "{{oferty_k}}", "{{oferty_l}}"), array($count_arr['oferty_b'], $count_arr['oferty_k'], $count_arr['oferty_l']), $boxy_html);

                        echo $boxy_html;
                        ?>
                    </div>
                </div>
                <div class="heroPhoto">
                    <img src="<?=THEME_PATH;?>/assets/images/aniaMainBg.png" class="img-responsive" alt="Anna Lucińska - konsultantka ds. finansów">
                </div>
                <!--<div class="heroDreams">
                    <ul id="dreamSlider">
                        <li><img src="<?=THEME_PATH;?>/assets/images/marzenie-auto.png"></li>
                        <li><img src="<?=THEME_PATH;?>/assets/images/marzenie-dom.png"></li>
                        <li><img src="<?=THEME_PATH;?>/assets/images/marzenie-remont.png"></li>
                        <li><img src="<?=THEME_PATH;?>/assets/images/marzenie-wakacje.png"></li>
                    </ul>
                </div>-->
            </div>
        </section>

        <section id="offerLogos">
            <div class="container">
                <ul class="list-inline" id="ofLogosSlider">
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/bank-bph.png" alt="Bank BPH"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/alior-bank.png" alt="Alior Bank"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/bank-smart.png" alt="Bank Smart"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/citi-handlowy.png" alt="City Handlowy"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/credi-agricole.png" alt="CrediAgricole"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/mbank.png" alt="mBank"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/deutshe-bank.png" alt="Deutsche Bank"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/pko.png" alt="PKO Bank Polski"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/euro-bank.png" alt="Euro Bank"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/bank-bph.png" alt="Bank BPH"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/alior-bank.png" alt="Alior Bank"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/bank-smart.png" alt="Bank Smart"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/citi-handlowy.png" alt="City Handlowy"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/credi-agricole.png" alt="CrediAgricole"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/mbank.png" alt="mBank"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/deutshe-bank.png" alt="Deutsche Bank"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/pko.png" alt="PKO Bank Polski"></li>
                    <li><img src="<?=THEME_PATH;?>/assets/images/ikony-banki/euro-bank.png" alt="Euro Bank"></li>
                </ul>
            </div>
        </section>

        <section id="bestOffers">
            <div class="container">
                <div class="boTitles text-center">
                    <h2 class="text-uppercase"><?=get_field('glowna_slider_tytul');?></h2>
                    <p><?=get_field('glowna_slider_podtytul');?></p>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="slider_arrows">
                            <div class="sla_left" id="slider_prev"><i class="icon-arrow-right"></i></div>
                            <div class="sla_right" id="slider_next"><i class="icon-arrow-right"></i></div>
                        </div>
                        <ul id="boSlider">
                        <?php
        				$loop = new WP_Query(array('post_type' => 'slajd', 'posts_per_page' =>4, 'orderby'=> 'ASC'));
                        $l = 0;
                        $btn_html = '';
                        while ( $loop->have_posts() )
                        {
                            $l++;
                            $loop->the_post();
                            ?>
                            <li class="boSlide">
                                <div class="row">
                                    <div class="col-xs-6 slider_image">
                                        <?php
                                        the_post_thumbnail('520x320');
                                        ?>
                                    </div>
                                    <div class="col-xs-6 sliderContentWrapper">
                                        <?php the_content();?>
                                        <a target="_blank" href="<?=get_field('slider_url');?>" class="btn btn-lg btn-warning pull-right">Sprawdź szczegóły &raquo;</a>
                                        <div class="clearfix"></div>
                                        <div class="text-right small offerNotice"><?=get_field('slider_dopisek');?></div>
                                    </div>
                                </div>
                            </li>
                            <?php

                            $btn_html .= '
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-default btn-block linkSlider '.($l==1?'active':'').'" data-slide="'.$l.'" id="linkSlider'.$l.'">'.get_the_title().'<span>'.get_field('slider_podpis').'</span></button>
                                </div>
                                ';

        			    }
            			wp_reset_query();
                        ?>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="sliderTabs">
                            <div class="row" >
                                <?php
                                echo $btn_html;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="activePromos">
            <div class="container">



                <div class="page-header">
                <a href="/promocje" class="pull-right linkMore text-uppercase">Zobacz więcej</a>
                <h2 class="text-uppercase">Aktualne promocje bankowe</h2>
                </div>

                <div class="row">
                    <?php
                    dynamic_sidebar( 'homepage_promocje' );
                    ?>
                </div>

            </div>
        </section>

        <section id="sectionArticles">
            <div class="container">
                <div class="page-header">
                <a href="/poradniki" class="pull-right linkMore text-uppercase">Zobacz więcej</a>
                <h2 class="text-uppercase">Poradnik finansowy</h2>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hidden-md hidden-sm hidden-xs articleBoxIntro">
                        <img src="<?=THEME_PATH;?>/assets/images/aniaArticlesBox.png" class="img-responsive" alt="Ania mówi">
                        <div class="spoke_cloud">Masz pytania dotyczące finansów? Tutaj znajdziesz odpowiedź!</div>
                    </div>

                    <?php
                    dynamic_sidebar( 'homepage_poradnik' );
                    ?>
                </div>
            </div>
        </section>

        <section id="activeOpinions">
            <div class="container">
                <div class="page-header">
                    <!--<a href="#" class="pull-right linkMore text-uppercase">Prześlij nam swoją opinię</a>-->
                    <h2 class="text-uppercase">Opinie naszych użytkowników</h2>
                </div>

                <div class="row">
                    <?=get_field('glowna_opinie');?>
                </div>
            </div>
        </section>

        <section id="otherServices">
            <div class="container">
                <div class="page-header">
                    <h2 class="text-uppercase text-center">Inne nasze serwisy</h2>
                </div>
                 <ul class="list-inline">
                     <li><a href="https://rankomat.pl" target="_blank"><img src="<?=THEME_PATH;?>/assets/images/inne-serwisy/logo-rankomat.png" alt="Rankomat"></a></li>
                     <li><a href="https://sowafinansowa.pl" target="_blank"><img src="<?=THEME_PATH;?>/assets/images/inne-serwisy/logo-sowa-finansowa.png" alt="Sowa Finansowa"></a></li>
                     <li><a href="https://komorkomat.pl" target="_blank"><img src="<?=THEME_PATH;?>/assets/images/inne-serwisy/logo-komorkomat.png" alt="Komórkomat"></a></li>
                     <li><a href="https://wakacyjnapolisa.pl/" target="_blank"><img src="<?=THEME_PATH;?>/assets/images/inne-serwisy/logo-wakacyjna-polisa.png" alt="Wakacyjna Polisa"></a></li>
                     <li><a href="http://wypowiadamoc.pl/" target="_blank"><img src="<?=THEME_PATH;?>/assets/images/inne-serwisy/logo-wypowiadam-oc.png" alt="Wypowiadam OC"></a></li>
                 </ul>
            </div>
        </section>
<?php
get_footer();
