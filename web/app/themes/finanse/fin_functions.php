<?php
add_action('init', 'create_feature');

function create_feature() {
    $feature_args = array(
        'labels' => array(
        'name' => __( 'Slider' ),
        'singular_name' => __( 'Slajd' ),
        'add_new' => __( 'Dodaj nowy slajd' ),
        'add_new_item' => __( 'Dodaj nowy slajd' ),
        'edit_item' => __( 'Edytuj slajd' ),
        'new_item' => __( 'Dodaj nowy slajd' ),
        'view_item' => __( 'Wyświetl slajd' ),
        'search_items' => __( 'Szukaj' ),
        'not_found' => __( 'Nie znaleziono slajdów' ),
        'not_found_in_trash' => __( 'Nie znaleziono slajdów w koszu' )
    ),
    'public' => true,
    'show_in_menu' => 'options-general.php',
    'show_ui' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'rewrite' => false,
    'menu_position' => 20,
    'supports' => array('title', 'editor', 'thumbnail')
    );
  register_post_type('slajd',$feature_args);
}
add_filter("manage_slajd_edit_columns", "feature_slajd_columns");

function slajd_edit_columns($feature_columns){
   $feature_columns = array(
      "cb" => "<input type=\"checkbox\" />",
      "title" => "Title",
   );
  return $feature_columns;
}

function pagination_bar($query=null) {
    
    if($query)
    {
    $wp_query = $query;
    }
    else
    {
    global $wp_query;    
    }
    
    $total_pages = $wp_query->max_num_pages;
     
    if ($total_pages > 1){
        
        echo '
        <nav class="text-center">
		    ';    
        
        $current_page = max(1, get_query_var('paged'));
        
        $paginate_links = paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => 'page/%#%',
            'prev_text' => '&laquo; Poprzednia',
            'next_text' => 'Następna &raquo;',
            'current' => $current_page,
            'total' => $total_pages,
            'type' => 'list'
        ));
 
        echo str_replace('page-numbers','pagination',$paginate_links);
        
        echo '
        </nav>';
    }
}

function count_offers() 
{
    global $wpdb;
    
    $q = "
        SELECT
            SUM(CASE WHEN typ_oferty = 'b' THEN 1 ELSE 0 END) oferty_b,          
            SUM(CASE WHEN typ_oferty = 'k' THEN 1 ELSE 0 END) oferty_k,          
            SUM(CASE WHEN typ_oferty = 'l' THEN 1 ELSE 0 END) oferty_l         
        FROM sowa_offers
        WHERE typ_oferty IN ('b','k','l') AND post_status = 'publish'       
    ";
    return $wpdb->get_row($q, ARRAY_A);  
}

function poradnik_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'poradnik_excerpt_length', 999 );

function add_lightbox_rel($content) {
       global $post;
       $pattern ='/<a(.*?)href=(\'|\")(.*?).(bmp|gif|jpeg|jpg|png)(\'|\")(.*?)>/i';
       $replacement = '<a$1href=$2$3.$4$5 class="clightbox"$6>';
       $content = preg_replace($pattern, $replacement, $content);
       return $content;
}
add_filter('the_content', 'add_lightbox_rel');