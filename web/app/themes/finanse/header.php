<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!--<link href="<?=THEME_PATH;?>/assets/bower_components/smartmenus/dist/addons/bootstrap/jquery.smartmenus.bootstrap.css" rel="stylesheet">-->
        <link href="<?=THEME_PATH;?>/assets/bower_components/lightslider/dist/css/lightslider.min.css" rel="stylesheet">
        <link href="<?=THEME_PATH;?>/assets/bower_components/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
        <link rel="stylesheet" href="<?=THEME_PATH;?>/assets/lesscompiler/compiler.php?files[]=../less/custom.less" type="text/css" media="all"/>
        <script src="<?=THEME_PATH;?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
        <?php        
        wp_head();         
        ?>  
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body <?php body_class(); ?>>
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NK74CH"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NK74CH');</script>
	<!-- End Google Tag Manager -->

        <nav class="navbar navbar-default navbar-static-top">
          <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=home_url()?>">
                <img alt="Rankomat finanse" src="<?=THEME_PATH;?>/assets/images/rankomatFinanseLogo.png">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="topmenu">                
                    
					<ul class="nav navbar-nav navbar-right header_social">
					<li><a href="http://www.facebook.com/rankomatpl" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a href="https://twitter.com/rankomat" target="_blank"><i class="icon-twitter"></i></a></li>
					<li><a href="https://plus.google.com/+rankomat" target="_blank"><i class="icon-google-plus"></i></a></li>
					</ul>
					<?php wp_nav_menu( array( 'container' => '', 'menu_class' => 'nav navbar-nav navbar-right', 'theme_location' => 'menu_gorne' ) ); ?>              
            </div>
          </div>
        </nav>

        <section id="mainMenu">
            <div class="container menuInner">
                <nav id="mainNav" role="navigation" class="collapse navbar-collapse">                
                    <?php wp_nav_menu( array( 'container' => '', 'menu_class' => 'nav navbar-nav', 'menu_id' => 'mainMenuList', 'theme_location' => 'menu_glowne', 'walker' => new Finanse_Nav_Menu()) ); ?>              
                    
                    <div class="visible-xs">
                        <?php wp_nav_menu( array( 'container' => '', 'menu_class' => 'nav navbar-nav navbar-right', 'theme_location' => 'menu_gorne' ) ); ?>              
                    </div>
                    
                </nav>
            </div>
        </section>