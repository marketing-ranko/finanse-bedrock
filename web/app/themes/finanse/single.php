<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

        <section id="mainContent" class="single_post">
            <div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-12 single_post_content">
						<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', get_post_format() );

							//the_post_navigation();

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								echo '<h3 class="comments_header">Komentarze:</h3>';
								comments_template( '', true );
							endif;

						endwhile; // End of the loop.
						?>

                        <?php dynamic_sidebar('sidebar_underpost');?>

					</div>
					<div class="col-md-3 col-sm-12 sidebar">
						<?php dynamic_sidebar('sidebar_common');?>
					</div>
				</div>
            </div>
        </section>
<?php

get_footer();
