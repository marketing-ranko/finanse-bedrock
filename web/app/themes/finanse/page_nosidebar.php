<?php
/**
*	Template Name: Strone bez sidebaru
*/

get_header(); ?>

        <section id="mainContent">
            <div class="container">
            
                <div class="col-sm-9">                
                    <?php
            		while ( have_posts() ) : the_post();

            			get_template_part( 'template-parts/content', get_post_format() );

            			//the_post_navigation();

            			// If comments are open or we have at least one comment, load up the comment template.
            			if ( comments_open() || get_comments_number() ) :
            				comments_template();
            			endif;

            		endwhile; // End of the loop.
            		?>
                    
                </div>               
            </div>
        </section>
<?php

get_footer();

