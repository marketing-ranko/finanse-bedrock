        <footer>
            <div class="container">

                <div class="row">
				
				    <div class="col-sm-6 footWidget">
                        
						<?php dynamic_sidebar( 'homepage_stopkaonas' );?>

                        <a href="http://www.facebook.com/rankomatpl" class="ikona_social"></a>


                        <p><a class="checkReviews" title="Prezentujemy treści wiadomości Klientów, którzy zakupili OC/AC za pośrednictwem naszej porównywarki" href="http://rankoteam.pl/opinie-o-rankomat-pl/">Sprawdź opinie o rankomat.pl</a></p>
						
						<div class="footer_socials">
							<a href="http://www.facebook.com/rankomatpl" target="_blank"><i class="icon-facebook"></i></a>
							<a href="https://twitter.com/rankomat" target="_blank"><i class="icon-twitter"></i></a>
							<a href="https://plus.google.com/+rankomat" target="_blank"><i class="icon-google-plus"></i></a>
						</div>
						
                    </div>
				
                    <div class="col-sm-3 footWidget">
                        <?php dynamic_sidebar( 'homepage_stopka1' );?>
						<?php dynamic_sidebar( 'homepage_stopka3' );?>
                    </div>
                    <div class="col-sm-3 footWidget">
                        <?php dynamic_sidebar( 'homepage_stopka2' );?>
						<?php dynamic_sidebar( 'homepage_stopka4' );?>
                    </div>
   

                </div>
				
				<div class="cookie">Ta strona używa cookie. <a href="https://rankomat.pl/pomoc/pliki-cookies-w-rankomatpl">Dowiedz się więcej o celu ich używania i zmianie ustawień cookie w przeglądarce</a>. Korzystając ze strony wyrażasz zgodę na używanie cookie, zgodnie z aktualnymi ustawieniami przeglądarki.</div>

            </div>



            <div class="subFooter copyright">
                <div class="container">
                    <div class="subfooter-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-8">Rankomat Sp. z o.o. Sp. k. 2009-<?=date("Y");?> | Wszystkie prawa zastrzeżone</div>
                                    <div class="col-sm-4">
                                        <?php wp_nav_menu(array( 'container' => '', 'menu_class' => 'list-inline pull-right', 'theme_location' => 'menu_stopka' )); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script src="<?=THEME_PATH;?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!--<script src="<?=THEME_PATH;?>/assets/bower_components/smartmenus/dist/jquery.smartmenus.min.js"></script>-->
        <!--<script src="<?=THEME_PATH;?>/assets/bower_components/smartmenus/dist/addons/bootstrap/jquery.smartmenus.bootstrap.min.js"></script>-->
        <script src="<?=THEME_PATH;?>/assets/bower_components/lightslider/dist/js/lightslider.min.js"></script>
        <script src="<?=THEME_PATH;?>/assets/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
        <script src="<?=THEME_PATH;?>/assets/js/custom.js"></script>
    </body>
</html>
