<?php
/**
 * finanse functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package finanse
 */

define('THEME_PATH', get_template_directory_uri());

if ( ! function_exists( 'finanse_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function finanse_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on finanse, use a find and replace
	 * to change 'finanse' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'finanse', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu_gorne' => esc_html__( 'Menu górne', 'finanse' ),
        'menu_glowne' => esc_html__( 'Menu główne', 'finanse' ),
        'menu_stopka' => esc_html__( 'Menu stopka', 'finanse' ),
	) );


    if (function_exists('add_theme_support')) {

        add_theme_support('post-thumbnails');

    	//Mandatory
    	add_image_size('100x100','100','100',true);
    	add_image_size('600x400','600','400',true);
    	add_image_size('800x400','800','400',true);
		add_image_size('800x250','800','250',true);
        add_image_size('360x200','360','200',true);
        add_image_size('520x320','520','320',true);
        add_image_size('500x240','500','240',true);

    }

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'finanse_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');

}
endif;
add_action( 'after_setup_theme', 'finanse_setup' );

class Finanse_Nav_Menu extends Walker_Nav_Menu {
  function start_lvl(&$output, $depth = 0, $args = array()) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"dropdown-menu megamenu\">\n";
  }
}

function fin_alter_cat_links($termlink, $term, $taxonomy)
{
    if ('category' == $taxonomy && 0 == $term->parent) {
        return str_replace('/category', '', $termlink);
    }
    return $termlink;
}
add_filter('term_link', 'fin_alter_cat_links', 10, 3);


function finanse_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'finanse_content_width', 640 );
}
add_action( 'after_setup_theme', 'finanse_content_width', 0 );

function finanse_widgets_init() {

    $common_params = array(
    'before_widget' => '<div class="box_widget %2$s" id="%1$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widget_title">',
	'after_title'   => '</h2>'
    );
    
    $args = array(
	'name'          => 'Sidebar - wspólny',
	'id'            => 'sidebar_common',
    );
    register_sidebar(array_merge($args,$common_params ));
    
    $common_params = array(
    'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '<h2 class="widget_title under_content">',
	'after_title'   => '</h2>'
    );
    
    $args = array(
	'name'          => 'Pod treścią',
	'id'            => 'sidebar_underpost',
    );
    register_sidebar(array_merge($args,$common_params ));

    $common_params = array(
    'before_widget' => ' ',
	'after_widget'  => ' ',
	'before_title'  => ' ',
	'after_title'   => ' '
    );

    $args = array(
	'name'          => 'Strona główna - aktualne promocje',
	'id'            => 'homepage_promocje',
    );
    register_sidebar(array_merge($args,$common_params ));

    $args = array(
	'name'          => 'Strona główna - poradnik finansowy',
	'id'            => 'homepage_poradnik',
    );
    register_sidebar(array_merge($args,$common_params ));
    


    $common_params = array(
    'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '<strong>',
	'after_title'   => '</strong>'
    );

    $args = array(
	'name'          => 'Stopka - box 1',
	'id'            => 'homepage_stopka1',
    );
    register_sidebar(array_merge($args,$common_params ));
	
    $args = array(
	'name'          => 'Stopka - box 2',
	'id'            => 'homepage_stopka2',
    );
    register_sidebar(array_merge($args,$common_params ));

    $args = array(
	'name'          => 'Stopka - box 3',
	'id'            => 'homepage_stopka3',
    );
    register_sidebar(array_merge($args,$common_params ));

    $args = array(
	'name'          => 'Stopka - box 4',
	'id'            => 'homepage_stopka4',
    );
    register_sidebar(array_merge($args,$common_params ));
	
	$args = array(
	'name'          => 'Stopka - box o nas',
	'id'            => 'homepage_stopkaonas',
    );
    register_sidebar(array_merge($args,$common_params ));	

}

add_action( 'widgets_init', 'finanse_widgets_init' );

function finanse_widget_content_wrap($content) {
    $content = '<div class="widget_content">'.$content.'</div>';
    return $content;
}
add_filter('widget_text', 'finanse_widget_content_wrap');

remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

/**
 * Enqueue scripts and styles.
 */
function finanse_scripts() {

}

add_action( 'wp_enqueue_scripts', 'finanse_scripts' );

require get_template_directory() . '/fin_functions.php';

// Widgety
require get_template_directory() . '/inc/widget-polecane.php';


// Force HTTPS redirect except redirect page
function force_ssl()
{    
    if (!is_ssl () && !is_page(REDIRECT_PAGE_ID))
    {
      header('HTTP/1.1 301 Moved Permanently');
      header("Location: https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
      exit();
    }
}

if(WP_ENV!='test')
{
add_action('template_redirect', 'force_ssl');    
}