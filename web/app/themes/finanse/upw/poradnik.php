<?php
/**
 * Standard ultimate posts widget template
 *
 * @version     2.0.0
 */
?>


  <?php if ($upw_query->have_posts()) : ?>

      <?php while ($upw_query->have_posts()) : $upw_query->the_post(); ?>

        <?php $current_post = ($post->ID == $current_post_id && is_single()) ? 'active' : ''; ?>

       <div class="col-xs-12 col-sm-4 col-lg-3 artBox">
            <div class="thumbnail">
              <a href="<?=the_permalink();?>"><?php
              the_post_thumbnail('500x240',array('class'=>'img-responsive','atl'=>get_the_title()));
              ?></a>
              <div class="caption">
                <h4><a href="<?=the_permalink();?>"><?=get_the_title();?></a></h4>
                <div class="boxDate"><?=get_the_time('j F Y');?></div>
                <p><?=get_the_excerpt();?></p>
                <a href="<?=the_permalink();?>" class="btn btn-success btn-sm pull-right" role="button">Czytaj więcej &raquo;</a>
                <div class="clearfix"></div>
              </div>
            </div>
        </div>

      <?php endwhile; ?>

  <?php else : ?>

    <p class="upw-not-found">
      <?php _e('No posts found.', 'upw'); ?>
    </p>

  <?php endif; ?>
