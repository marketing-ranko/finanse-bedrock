<?php
/**
 * Standard ultimate posts widget template
 *
 * @version     2.0.0
 */
?>


  <?php if ($upw_query->have_posts()) : ?>

      <?php while ($upw_query->have_posts()) : $upw_query->the_post(); ?>

        <?php $current_post = ($post->ID == $current_post_id && is_single()) ? 'active' : ''; ?>           
        <div class="promo_row">
            <div class="row">
                <div class="col-xs-12">
                    <a href="<?=the_permalink();?>">                        
                        <?php
                        the_post_thumbnail('250x120',array('class'=>'img-responsive'));
                        ?>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 promo_title"><a href="<?=the_permalink();?>"><?=get_the_title();?></a></div>
            </div>        
        </div>  

      <?php endwhile; ?>

  <?php else : ?>

    <p class="upw-not-found">
      <?php _e('No posts found.', 'upw'); ?>
    </p>

  <?php endif; ?>