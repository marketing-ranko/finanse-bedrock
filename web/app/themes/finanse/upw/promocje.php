<?php
/**
 * Standard ultimate posts widget template
 *
 * @version     2.0.0
 */
?>


  <?php if ($upw_query->have_posts()) : ?>

      <?php while ($upw_query->have_posts()) : $upw_query->the_post(); ?>

        <?php $current_post = ($post->ID == $current_post_id && is_single()) ? 'active' : '';

        $promocja_bonus = get_field('promocja_bonus');
        ?>

        <div class="col-xs-12 col-sm-6 col-lg-3 promoBox">
            <div class="thumbnail">
            <?php
            if($promocja_bonus) echo '<div class="promoBonus">'.$promocja_bonus.'</div>';
            ?>
              <a href="<?=the_permalink();?>"><?php
              the_post_thumbnail('500x240',array('class'=>'img-responsive','alt'=>get_the_title()));
              ?></a>
              <div class="caption">
                <h4><a href="<?=the_permalink();?>"><?=get_the_title();?></a></h4>
                <div class="boxDate"><?=get_the_time('j F Y');?></div>
                <p><?=get_the_excerpt();?></p>
                <a href="<?=the_permalink();?>" class="btn btn-danger btn-sm pull-right" role="button">Zobacz promocję &raquo;</a>
                <div class="clearfix"></div>
              </div>
            </div>
        </div>

      <?php endwhile; ?>

  <?php else : ?>

    <p class="upw-not-found">
      <?php _e('No posts found.', 'upw'); ?>
    </p>

  <?php endif; ?>
