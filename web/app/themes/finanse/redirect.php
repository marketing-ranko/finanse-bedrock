<?php 
/*
 *	Template Name: Sowaconnect - strona przekierowania
 */
 ?>
<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title(); ?></title>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			.sowaTopRedirect { margin-top: 200px; }
			.sowaTopRedirect h3 { color: #5E95C5; font-size: 32px;}
			.sowaTopRedirect p { margin-top: 30px; font-size: 11px; }
			#licznik { font-weight: bold;}			
            #odliczanie {
                height: 200px;
                width: 200px;
                margin: 0 auto;
            }
            #odliczanie > svg {
                height: 100%;
                display: block;
            }		            
            #odliczanie .progressbar-text { 
                font-size:50px;
            }
		</style>
		<script type="text/javascript" src="https://finanse.rankomat.pl/wp-content/themes/finanse/assets/js/progressbar.min.js"></script>
		
	</head>
	<body>
		<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NK74CH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NK74CH');</script>
<!-- End Google Tag Manager -->

		<?php 
        
        global $wpdb, $wp_query;
    
        $ref_link = $wp_query->query_vars['ref_link'];
        
        //v("RFL:".$ref_link);
        
        //$ref_link = $_GET['ref_link'];
        
        $rf_arr = explode("-", $ref_link);
        
        $alias = $rf_arr[0];
        
        if (trim($alias)) {
            $link_arr = $wpdb->get_row("
    		SELECT *
    		FROM ".$wpdb->prefix."sowalinki			
    		WHERE sl_alias = '" . trim($alias) . "'
    		", ARRAY_A);
        }
        
        if ($link_arr) {
            $redirect_arr = array('sl_url'=>$link_arr['sl_url'],'sl_datalayer'=>$link_arr['sl_datalayer']);            
        } else {   
            //v("Redirect link not found");
            wp_redirect(site_url(), 301);
        }        
        		
        $redirectCustomUrl = $redirect_arr['sl_url'];        
        
        if($_COOKIE['_ga'])
        {
            $cga_arr=explode(".",$_COOKIE['_ga']);
            $cga_arr=array_slice($cga_arr,-2,2);         
            $cId = implode('.',$cga_arr);
        }
                
        if(!$cId)
        {
            $cId = '888'.mt_rand(100000, 999999);
        }
        
        if(stripos($redirectCustomUrl,'alfakredyt')!==false)
        {
            $redirectCustomUrl .= '&_ga='.$_COOKIE['_ga'];    
        }     
        
        if(stripos($redirectCustomUrl,'track.adtraction.com')!==false || stripos($redirectCustomUrl,'tradedoubler.com')!==false)
        {
            $redirectCustomUrl .= '&epi='.$cId; 
        }
        elseif(stripos($redirectCustomUrl,'novem')!==false)
        {
            $redirectCustomUrl .= '?aff_sub1='.$cId; 
        }
        elseif(stripos($redirectCustomUrl,'tracking.affizzy.com')!==false || stripos($redirectCustomUrl,'affiliate44.com')!==false)
        {
            $redirectCustomUrl .= '&aff_sub='.$cId; 
        }
        elseif(stripos($redirectCustomUrl,'filarum.pl')!==false || stripos($redirectCustomUrl,'sohocredit.pl')!==false)
        {
            $redirectCustomUrl .= '&data1='.$cId.'&data2=SowaFinansowa'; 
        }
        elseif(stripos($redirectCustomUrl,'system3secure.pl')!==false)
        {
            $redirectCustomUrl .= '&s2='.$cId; 
        }   
        elseif(stripos($redirectCustomUrl,'extraportfel')!==false)
        {
            $redirectCustomUrl .= '&data1='.$cId; 
        }  
        elseif(stripos($redirectCustomUrl,'lendon.pl')!==false)
        {
            $redirectCustomUrl .= '&data1='.$cId; 
        }     
        elseif(stripos($redirectCustomUrl,'ekspreskasa.pl')!==false)
        {
            $redirectCustomUrl .= '?transaction_id='.$cId; 
        } 
        elseif(stripos($redirectCustomUrl,'netcredit.pl')!==false)
        {
            $redirectCustomUrl .= '?epi='.$cId; 
        }
        else
        {
            $redirectCustomUrl .= '&transaction_id='.$cId; 
        }
                
        if($redirect_arr['sl_datalayer'])
        {
            $dl_arr = unserialize($redirect_arr['sl_datalayer']);
        }
        
        if($dl_arr && is_array($dl_arr))
        {

            echo "
            <script>
                dataLayer.push ({
                    'event':'redirectDataPass',
                    ".($cId?"'clientId':'".$cId."',":"")."                    
                    'transactionId':'".(session_id())."',
                    ";
            
            $arr_dlvs = array(); 
            
            for($i=1;$i<6;$i++)
            {
                if($dl_arr['value'.$i])
                {
                    if($dl_arr['key'.$i]=='timeGoesBy')
                    {
                    $arr_dlvs[] = "'".$dl_arr['key'.$i]."':".str_replace(',','.',$dl_arr['value'.$i])."";    
                    }
                    else
                    {
                    $arr_dlvs[] = "'".$dl_arr['key'.$i]."':'".$dl_arr['value'.$i]."'";
                    }
                }
            }
            
            if($arr_dlvs)
            {
                echo implode(",",$arr_dlvs);
            }
                    
            echo "  });            
            </script>            
            ";
        }                                     		
		?>

		<div class="sowaTopRedirect container text-center" style="display:none">
			<h3>Życzymy Ci szybkiej akceptacji wniosku! </h3>
            
			<p>Przekierowanie nastąpi za : <span id="licznik"> </span></p>
			<div id="odliczanie"></div>		
            <div class="container text-center text_redirect" style="display:none">Jeśli przeglądarka nie przekieruje Cię automatycznie <strong><a href="<?=$redirectCustomUrl;?>">kliknij tutaj</a></strong> aby przejść do strony pożyczkodawcy.</div>	
		</div>

		<script type="text/javascript">			
                    
            document.getElementsByClassName('sowaTopRedirect')[0].style.display='block';
				
			var odlicz_max = 6;
			var odlicz = 6;		
			
			var redirectCustomUrl = '<?= $redirectCustomUrl ?>';
            var env = '<?= $_SESSION["env"] ?>';
			var element = document.getElementById('odliczanie');

			var seconds = new ProgressBar.Circle(element, {
			    duration: 200,
			    color: "#6BC25D",
			    trailColor: "#ddd"
			});
			
			var redirect_ready = 0;
			
			if(!redirectCustomUrl)
			{
				window.location = site_url();
			}
			else
			{
				var Interval = setInterval(function() {
					odlicz--;
					
					if(odlicz<0)
				    {
						odlicz = 0;
					}
					
					var progress = (odlicz_max - odlicz)/odlicz_max;				
								    
				    seconds.animate(progress, function() {
				        seconds.setText(odlicz);
				    });		    
				    			   	
				    if(dataLayer[2].event=='gtm.load')
				    {
				    redirect_ready = 1;	
				    }
                    
                    if(dataLayer[3])
                    {
                        if(dataLayer[3].event=='gtm.load')
    				    {
    				    redirect_ready = 1;	
    				    }
                    }
				    
					if (redirect_ready == 1 || odlicz===0) {
                        if(env!='test')
                        {
                        window.location = redirectCustomUrl;    
                        //console.log("Redirect");
                        document.getElementsByClassName('text_redirect')[0].style.display='block';
                        }						
						clearInterval(Interval);
						//console.log('REDIRECT');
					}
				}, 1000);
			}			
	
		</script>
        <noscript><div class="container text-center">Jeśli przeglądarka nie przekieruje Cię automatycznie <strong><a href="<?=$redirectCustomUrl;?>">kliknij tutaj</a></strong> aby przejść do strony pożyczkodawcy.</div></noscript>
		<script src="//code.jquery.com/jquery.js"></script>			
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</body>
</html>