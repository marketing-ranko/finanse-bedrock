$(document).ready(function() {
    
    $(".btn").mouseup(function(){
        $(this).blur();
    })
    
    var boSlider = $("#boSlider").lightSlider({
        item: 1,
        pager: false, 
        loop: true,
        pause: 4000,
        auto: true,
        onAfterSlide: function (el) {
            var slide_index = el.getCurrentSlideCount();
            $('.linkSlider').removeClass('active');
            $('#linkSlider'+slide_index).addClass('active');
        } 
    });
    
    var dreamSlider = $("#dreamSlider").lightSlider({
        item: 1,
        mode: 'fade',
        auto: true,
        pause: 4000,
        speed: 1500,
        loop: true,
        pager: false
    });
    
    var ofLogosSlider = $("#ofLogosSlider").lightSlider({
        item: 1,
        auto: true,
        pause: 2000,
        autoWidth: true,
        speed: 1500,
        slideMargin: null,
        loop: true,
        pager: false
    });   
    
    $('.clightbox').magnificPopup({
      type: 'image'
    });
    
    
    $( "body" ).on( "click", ".linkSlider", function(e) {    
        var slide_index = $(this).data('slide');
        boSlider.goToSlide((slide_index));           
        $('.linkSlider').removeClass('active');
        $(this).addClass('active');        
    });
    
    $( "body" ).on( "click", "#slider_prev", function(e) {    
        boSlider.goToPrevSlide();
    });
    
    $( "body" ).on( "click", "#slider_next", function(e) {    
        boSlider.goToNextSlide();
    });
    
    $(".menu-item-has-children").hover(
        function() { $('.dropdown-menu', this).stop().fadeIn("fast");
        },
        function() { $('.dropdown-menu', this).stop().fadeOut("fast");
    });
	
	$('.share-facebook').click( function(e) {
        e.preventDefault();
        window.open(
          'https://www.facebook.com/sharer/sharer.php?u='+window.location.href,
          'facebook-share-dialog',
          'width=800,height=600'
          );
        return false;
    });

    $('.share-twitter').click( function(e) {
        e.preventDefault();
        window.open(
          'https://twitter.com/home?status='+window.location.href,
          'twitter-share-dialog',
          'width=800,height=600'
          );
        return false;
    });

    $('.share-google-plus').click( function(e) {
        e.preventDefault();
        window.open(
          'https://plus.google.com/share?url='+window.location.href,
          'gplus-share-dialog',
          'width=800,height=600'
          );
        return false;
    });
    
});