<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require 'lessphp/Cache.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);
$path = realpath(dirname(__FILE__));
Less_Cache::$cache_dir = $path.'/cache/';

$files_compile = array();

if(!$_GET['files'] || !is_array($_GET['files']))
{
    die('Błąd: w parametrach nie podano plików do skompilowania');
}

foreach($_GET['files'] as $file)
{
    $files_compile[$file] = '../css/';    
}

$options = array( 'compress'=>true );

$css_file_name = Less_Cache::Get($files_compile,$options);
$compiled = file_get_contents(Less_Cache::$cache_dir.$css_file_name );
header("Content-type: text/css", true);
echo $compiled;
?>