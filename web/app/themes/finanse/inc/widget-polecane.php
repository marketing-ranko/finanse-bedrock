<?php
/**
 * Plugin Name: Polecane oferty
 * Description: Wyświetlanie widgetu z polecanymi ofertami.
 * Version: 1.0
 *
 */
 
 $offer_types_tag = array(
    'konta' => array('name'=>'konta','type'=>'k'),
    'lokaty' => array('name'=>'lokaty','type'=>'l'),
    'kredyty' => array('name'=>'kredyty gotówkowe','type'=>'b')
 );

add_action( 'widgets_init', 'polecaneof' );
if( !function_exists('polecaneof') ){
	function polecaneof() {
		register_widget( 'polecaneof' );
	}
}
if( !class_exists('polecaneof') ){
	class polecaneof extends WP_Widget{

		// Initialize the widget
		function __construct() {
			parent::__construct(
				'polecaneof', 
				'Polecane oferty', 
				array('description' => 'Automatycznie generuje listę polecanych ofert'));  
		}

		// Output of the widget
		function widget( $args, $instance ) {
			global $post, $offer_types_tag, $wpdb;	
				
			$title = apply_filters( 'widget_title', $instance['title'] );			
            $wstep = $instance['wstep'];
			$num_fetch = ($instance['num_fetch']?$instance['num_fetch']:5);
            
            $current_post_tags = get_the_tags();                      
            if($current_post_tags)
            {
                foreach($current_post_tags as $tag)
                {
                    if($offer_types_tag[$tag->slug])
                    {
                        $type_data = $offer_types_tag[$tag->slug];
                    }
                }    
            }   
            else
            {
                return;
            }         
            
            if($type_data)
            {
                $title = $title.' '.$type_data['name'];
                    			
    			// Opening of widget
    			echo $args['before_widget'];
    			
    			// Open of title tag
    			if( !empty($title) ){ 
    				echo $args['before_title'] . $title . $args['after_title']; 
    			}
    				
    			// Widget Content
    			$current_post = $post->ID;		            
                $lista_rank = array();
                
                if($wstep) {
                    echo '<div class="polecaneof_wstep">'.$wstep.'</div>';
                }               
                
                $sqlWhere[] = "(post_status = '' OR post_status = 'publish') AND typ_oferty = '".$type_data['type']."'";
                
                $q = "
                SELECT * FROM sowa_offers
                ".($sqlWhere?"WHERE ".implode(" AND ",$sqlWhere):"")."        
                ORDER BY menuorder ASC
                LIMIT ".($num_fetch?$num_fetch:'5')."
                ";
                 
                //v($q);                
                $rows_db = $wpdb->get_results($q,ARRAY_A);  
                
                //v($rows_db);
                
                foreach($rows_db as $fields)
                {
                    
                    if(($fields['post_type']=='chwilwki' || !$fields['post_type']) && !$use_old_code)
                    {    
                    $fields['koszt_chwilowki'] = $fields['koszt_chwilowki_n'];
                    $fields['url_official_website_prod'] = str_replace('http://www.','',$fields['url_official_website_prod']);
                    $fields['url_official_website_prod'] = str_replace('https://www.','',$fields['url_official_website_prod']);
                    $fields['url_cta'] = str_replace('http://www.sowafinansowa.pl/',unssl(site_url()).'/',$fields['url_cta']);
                    $fields['url_cta'] = str_replace('http://sowafinansowa.pl/',unssl(site_url()).'/',$fields['url_cta']);
                    $fields['url_cta'] = str_replace('https://sowafinansowa.pl/',unssl(site_url()).'/',$fields['url_cta']);                        
                    }    
                    elseif($fields['post_type']=='lokaty')
                    {            
                    $fields['url_cta'] = str_replace('http://sowafinansowa.pl/',unssl(site_url()).'/',$fields['lok_url']);
                    $fields['url_cta'] = str_replace('https://sowafinansowa.pl/',unssl(site_url()).'/',$fields['url_cta']);
                      
                    }    
                    elseif($fields['post_type']=='konta')    
                    {    
                    $fields['url_cta'] = str_replace('http://sowafinansowa.pl/',unssl(site_url()).'/',$fields['konto_url_cta']);
                    $fields['url_cta'] = str_replace('https://sowafinansowa.pl/',unssl(site_url()).'/',$fields['url_cta']);                        
                    } 
                    
                    $fields['url_cta'] = str_replace('http://www.sowafinansowa.pl/',unssl(site_url()).'/',$fields['url_cta']);
                    $fields['url_cta'] = str_replace('http://sowafinansowa.pl/',unssl(site_url()).'/',$fields['url_cta']);
                    $fields['url_cta'] = str_replace('https://sowafinansowa.pl/',unssl(site_url()).'/',$fields['url_cta']);
                    
                    echo '<div class="row">
                                <div class="col-xs-5"><a href="'.$fields['url_cta'].'" target="_blank"><img src="'.$fields['logo'].'" class="img-responsive"></a></div>
                                <div class="col-xs-7">                                
                                    <a href="'.$fields['url_cta'].'" target="_blank"> <strong>'.$fields['tytul'].'</strong></a>                                
                                </div>
                            </div>';
                }
                                
    			wp_reset_postdata();
    			// Closing of widget
    			echo $args['after_widget'];	    
            }            
		}

		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';			
            $wstep = isset($instance['wstep'])? $instance['wstep']: '';
			$num_fetch = isset($instance['num_fetch'])? $instance['num_fetch']: 5;			
			?>

			<!-- Text Input -->
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">Tytuł</label> 
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</p>	
            
            <!-- Wstęp -->
			<p>
				<label for="<?php echo $this->get_field_id('wstep'); ?>">Wstęp</label><br/>                 
                <textarea class="widefat" id="<?php echo $this->get_field_id('wstep'); ?>" name="<?php echo $this->get_field_name('wstep'); ?>"><?php echo $wstep; ?></textarea>
			</p>	
			
			<!-- Show Num --> 
			<p>
				<label for="<?php echo $this->get_field_id('num_fetch'); ?>">Ilość</label>
				<input class="widefat" id="<?php echo $this->get_field_id('num_fetch'); ?>" name="<?php echo $this->get_field_name('num_fetch'); ?>" type="text" value="<?php echo $num_fetch; ?>" />
			</p>

		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);			
            $instance['wstep'] = (empty($new_instance['wstep']))? '': $new_instance['wstep'];
			$instance['num_fetch'] = (empty($new_instance['num_fetch']))? '': strip_tags($new_instance['num_fetch']);

			return $instance;
		}	
	}
}
?>